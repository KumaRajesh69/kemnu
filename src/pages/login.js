import React from 'react'
import { Box, Button, Grid, Hidden, IconButton, InputAdornment, InputLabel, OutlinedInput, Stack, TextField, Typography } from '@mui/material'
import Link from 'next/link'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Router from "next/router";


function Login() {
    const [showPassword, setShowPassword] = React.useState(false);

    // const [email, setEmail] = useState('');
    // const [emailError, setEmailError] = useState('');
    // const [password, setPassword] = useState('');
    // const [passwordError, setPasswordError] = useState('');

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    // const validateFilds = () => {
    //     if(email === ''){
    //         setEmailError('Please Enter a valid email');
    //         return false;
    //     }else if(password === ''){
    //         setpasswordError('Please Enter a valid password');
    //         return false;
    //     }else {
    //         return true;
    //     }
    // }

    // const handleLogin = () =>{
    //     if (validateFilds()){

    //     }
    // }
    return (
        <>
            <Grid container spacing={2} width='100%' height='100vh' >
                <Grid item md='6' sm='0'>
                    <Box sx={{
                        height:'100vh auto',width:'100%'
                    }}>
                    <img src='/images/login-bgimage.svg' width='100%' height='620px' />
                    </Box>
                </Grid>

                <Grid item md='6' xs='12' width='100%'>
                    <Stack sx={{
                        mx:{md:10,xs:1},mt:{md:12,xs:1}
                    }}>
                        <Hidden smUp>
                        <img src='/images/Vector.svg' width='100%' />
                        </Hidden>
                    <Box >
                        <Typography  sx={{ color:'#4B1C7C',fontSize:20,fontWeight:'bold' }}>
                            Login
                        </Typography>
                        <Typography sx={{ fontSize:15,mt:1 }}     >
                            Sign in to your account
                        </Typography>
                    </Box>

                    <Box sx={{mt:3}} >
                        <Typography  sx={{fontSize:15, color: '#818181',}}>
                            Email ID
                        </Typography>
                        <TextField
                            variant='outlined'
                            type='text'
                            sx={{ mb: 2 }}
                            // error={!!emailError}
                            // helperText={emailError}
                            fullWidth
                        /><Box sx={{mt:2}} >
                        <Typography sx={{fontSize:15, color: '#818181',}}>
                            Password
                        </Typography>
                        <TextField
                            variant='outlined'
                            type={showPassword ? 'text' : 'password'}
                            // error={!!passwordError}
                            // helperText={passwordError}
                            InputProps={{
                                endAdornment: <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                                    </IconButton>
                                </InputAdornment>,
                            }}
                            sx={{ marginBottom: 2 }}
                            fullWidth
                        />
                        </Box>
                        <Typography  sx={{fontSize:15, color: '#818181',}} >
                        By creating an account or logging in, you agree to our Terms & Privacy Policy.
                        </Typography>
                    </Box>
                    <Stack sx={{mt:3}} >
                   
                        <Button variant='contained'
                         onClick={(e) => {
                            Router.replace("/home");
                        }}
                            bgcolor='3C50E0' sx={{ padding: 2, font: 'Rubik', fontStyle: 'normal' }}
                        >
                            Sign In
                        </Button>

                        <Link href="#" underline="hover" style={{ textAlign: 'right', marginTop: 10 }}>
                            Forgot Password?
                        </Link>
                    </Stack>
                    </Stack>

                </Grid>
            </Grid>
        </>
    )
}

export default Login