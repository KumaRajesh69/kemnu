import { Box } from '@mui/material'
import Login from './login'

// const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <Box sx={{ bgcolor: 'white' }}>
    <Login />
    </Box>
  )
}
